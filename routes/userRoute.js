const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");



router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/authentication", (req, res) => {
	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));
});


router.post("/createorder", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}
	userController.createOrder(data).then(resultFromController => res.send(resultFromController));
});


router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});

module.exports = router;





