const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");

const auth = require("../auth");

// create product (admin only)
router.post("/createproduct", auth.verify, (req, res) => {

	const data = {
		products: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.createProductAdmin(data).then(resultFromController => res.send(resultFromController));
});

// Retrieve all active products
router.get("/activeproducts" , (req, res) => {
	productController.activeProducts().then(resultFromController => res.send(resultFromController));
});

// Retrieve specific product
router.get("/:productId", (req, res) => {
	productController.specifiedProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Update product (admin only)
router.put("/:productId", auth.verify, (req,res) => {
	const data = {
		products: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProductAdmin(data, req.params).then(resultFromController => res.send(resultFromController));
});

// Archive product (admin only)
router.patch("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		products: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProductAdmin(data, req.params).then(resultFromController => res.send(resultFromController));
});





module.exports = router;



