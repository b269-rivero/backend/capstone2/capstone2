const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Register
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user,error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};

// Authentication
module.exports.authenticateUser = (reqBody, res) => {
		return User.findOne({email: reqBody.email}).then(result =>{
			if (result == null){
				return false
			} else {
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
				if (isPasswordCorrect) {
					return {access: auth.createAccessToken(result)}
				} else {
					return false;
				};
			};
		});
	};


// Create order
// 	module.exports.createOrder = async (data) => {
// 	let isUserUpdated = await User.findById(data.userId).then(user => {
// 		user.orders.push({productId: data.productId});
// 		return user.save().then((user, error) => {
// 			if (error){
// 				return false;
// 			} else {
// 				return true;
// 			};
// 		});
// 	});
// 	let isProductUpdated = await Product.findById(data.productId).then(product => {
// 		product.orders.push({userId: data.userId});
// 		return product.save().then((product, error) => {
// 			if (error){
// 				return false;
// 			} else {
// 				return true;
// 			};
// 		});
// 	});
// 	if(isUserUpdated && isProductUpdated){
// 		return true;
// 	} else {
// 		return false;
// 	};
// };

module.exports.createOrder = async (data) => {
    let isUserUpdated = await User.findById(data.userId).then(user => {
        user.orders.push({productId: data.productId, productName: data.productName});
        return user.save().then((user, error) => {
            if (error){
                return false;
            } else {
                return true;
            };
        });
    });
    let isProductUpdated = await Product.findById(data.productId).then(product => {
        product.orders.push({userId: data.userId});
        return product.save().then((product, error) => {
            if (error){
                return false;
            } else {
                return true;
            };
        });
    });
    if(isUserUpdated && isProductUpdated){
        return true;
    } else {
        return false;
    };
};





// Retreive user details
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		if (result == null) {
			return false
		} else {
			result.password = "*****"
			return result
		}
	})
};
