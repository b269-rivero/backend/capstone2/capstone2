const Product = require("../models/Product");

// create product (admin only)
module.exports.createProductAdmin = (data) => {
	if (data.isAdmin) {
		let newProduct = new Product({
			name : data.products.name,
			description : data.products.description,
			price : data.products.price
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};



// Retrieve all active products
module.exports.activeProducts = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	});
};

// Retrieve specific product
module.exports.specifiedProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

// Update product (admin only)
module.exports.updateProductAdmin = (data, reqParams) => {
	if (data.isAdmin) {
			let updatedProduct = {
			name : data.products.name,
			description : data.products.description,
			price : data.products.price
		};
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error) {
				return false;
			} else {
				return true
			};
		});
	};
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};


// Archive product (admnin only)
module.exports.archiveProductAdmin = (data, reqParams) => {
	if (data.isAdmin){
		let updateActiveField = {
		isActive: false
	};
		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};




